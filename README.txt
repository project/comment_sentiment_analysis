Description
-----------
This module will help you to analyze your comment text in a better way by
classifying the comments as positive/negative/neutral. Sentiment analysis is
performed only on comment_body on comment_subject field.

It also generates a pie chart report on sentiments data using Highcharts plugin.

Prerequisites
-------------
You must Sign-up on http://www.datumbox.com for generating API credentials.


Installation
------------
Download the module and enable it.

Configuration
-------------
1. To configure it, go to admin/comment-sentiment-analysis-settings and provide
sentiment analysis API URL and corresponding key which you get from Datumbox
(http://www.datumbox.com).

2. You can also select content types on which you want comment analysis to be
allowed.

3. Configure no. of comments to be process on per cron request.

API
---
1. To get the API key, click on "API ACCESS" link on your profile drop-down
menu.
2. Copy the API key

POST /1.0/SentimentAnalysis.json
Parameter:
api_key
text

Authors
-------
Pushpendra Kumar (https://www.drupal.org/u/mepushpendra)

If you use this module, find it useful, and want to send the authors a thank you
note, then use the Feedback/Contact page at the URLs above.
