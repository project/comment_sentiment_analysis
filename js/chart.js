(function ($) {
  
  $('#chart-container').highcharts({
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45
        }
      },
      title: {
        text: ''
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      colors: [
        '#4572A7',
        '#AA4643',
        '#89A54E',
      ],
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45,
          showInLegend: true,
          colorByPoint: true,
          point:{
            events : {
              legendItemClick: function (e) {
                e.preventDefault();
              }
            }
          },
        }
      },
      series: [{
        name: 'Comment Sentiments',
        data: Drupal.settings.comment_sentiment_analysis.data
      }]
    });
})(jQuery);
