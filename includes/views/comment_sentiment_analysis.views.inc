<?php

/**
 * @file
 * Expose table comment_analysis_data to views.
 */

/**
 * Implements hook_views_data().
 */
function comment_sentiment_analysis_views_data() {
  $data['comment_analysis_data']['table']['group'] = t('Comment Analysis Data table');
  $data['comment_analysis_data']['table']['base'] = array(
    'title' => t('Comment Analysis Data table'),
    'help' => t('Comment Analysis Data contains comment analysis data and can be related to comment.'),
    'weight' => -10,
  );
  // The Result Text field.
  $data['comment_analysis_data']['result_text'] = array(
    'title' => t('Result Text'),
    'help' => t('The Result Text returned by API.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['comment_analysis_data']['table']['join'] = array(
    'comment' => array(
      'left_field' => 'cid',
      'field' => 'cid',
    ),
  );
  $data['comment_analysis_data']['cid'] = array(
    'title' => t('comment-id'),
    'help' => t('record comment-id'),
    'field' => array(
      'handler' => 'views_handler_field_comment',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'comment',
      'field' => 'cid',
      'handler' => 'views_handler_relationship',
      'label' => t('comment'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_comment_cid',
      'numeric' => TRUE,
      'validate type' => 'cid',
    ),
  );
  $data['comment_analysis_data']['nid'] = array(
    'title' => t('Node-id'),
    'help' => t('record node-id'),
    'field' => array(
      'handler' => 'views_handler_field_node',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  return $data;
}
