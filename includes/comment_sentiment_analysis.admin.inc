<?php

/**
 * @file
 * Sentiment analysis admin settings.
 */

// Default API URL.
define("DEFAULT_API_URL", 'http://api.datumbox.com/1.0/SentimentAnalysis.json');

/**
 * Callback function for form.
 *
 * @param array $form
 *   Form Array.
 * @param array $form_state
 *   Form State Array.
 *
 * @return type array
 *   Form Array.
 */
function comment_sentiment_analysis_api_settings($form, &$form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process Pending/New Comments'),
    '#submit' => array('comment_sentiment_analysis_batch_form_submit'),
  );

  $form['comment_sentiment_analysis_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#default_value' => variable_get('comment_sentiment_analysis_api_url', DEFAULT_API_URL),
    '#maxlength' => 128,
    '#description' => t("REST API URL from !api_url", array('!api_url' => '<a href="http://www.datumbox.com/">Datumbox</a>')),
  );

  // Get all node types.
  $node_types = node_type_get_types();
  $options = array();
  foreach ($node_types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['comment_sentiment_analysis_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('comment_sentiment_analysis_api_key', ''),
    '#maxlength' => 128,
    '#description' => t('!api_key from !url', array(
      '!api_key' => '<a href="http://www.datumbox.com/apikeys/view/">API Key</a>',
      '!url' => '<a href="http://www.datumbox.com/">Datumbox</a>',
    )
    ),
  );

  $form['comment_sentiment_analysis_allowed_ctypes'] = array(
    '#type' => 'select',
    '#title' => t('Select Content Types on which you want to perform sentiment analysis'),
    '#options' => $options,
    '#default_value' => variable_get('comment_sentiment_analysis_allowed_ctypes', ''),
    '#multiple' => TRUE,
  );
  $options_cron = array(10, 20, 50, 100, 200, 500, 1000);
  $form['comment_sentiment_analysis_process_per_cron'] = array(
    '#type' => 'select',
    '#title' => t('How many comments do you want to process per cron run?'),
    '#options' => array_combine($options_cron, $options_cron),
    '#default_value' => variable_get('comment_sentiment_analysis_process_per_cron', COMMENT_SENTIMENT_ANALYSIS_COMMENTS_TOBE_PROCESSED),
  );
  return system_settings_form($form);
}

/**
 * Submit callback for batch_form.
 *
 * @param array $form
 *   Form Array.
 * @param array $form_state
 *   Form State Array.
 */
function comment_sentiment_analysis_batch_form_submit($form, $form_state) {
  batch_set(comment_sentiment_analysis_send_comment());
}

/**
 * Callback for batch_set.
 *
 * @return array
 *   Array of batches.
 */
function comment_sentiment_analysis_send_comment() {
  $nodes = comment_sentiment_analysis_nodes_tobe_processed();
  $pending_comments = array();
  if (!empty($nodes)) {
    drupal_set_message(t('Updating Comment Sentiments'));
    foreach ($nodes as $node) {
      $comment_result = db_query('SELECT c.cid FROM {comment} c
                                  LEFT JOIN {comment_analysis_data} cad 
                                  ON c.cid = cad.cid 
                                WHERE c.nid = :nid 
                                  AND cad.cid IS NULL', array(':nid' => $node->nid));
      while ($record = $comment_result->fetchObject()) {
        $comment = comment_load($record->cid);
        $pending_comments = array(
          'cid' => $record->cid,
          'nid' => $comment->nid,
          'title' => $comment->comment_body[LANGUAGE_NONE][0]['value'],
        );
        $operations[] = array('comment_sentiment_analysis_update_sentiment', array($pending_comments));
      }
    }
  }
  else {
    drupal_set_message(t('No comment found for processing, please add some.'), 'warning');
  }

  if (isset($operations)) {
    $batch = array(
      'operations' => $operations,
      'finished' => 'comment_sentiment_analysis_update_finished',
    );
    return $batch;
  }
}
